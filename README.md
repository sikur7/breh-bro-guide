# Breh-Bro-Guide

Hello, Bro. In this guide you will find detailed explanations and command refrences needed to fulfill the following responsibilities inherited by the 'Bro' role:
*Player engagement/Being responsive in chat
*Fix grief
*Respond to modreqs
*Issuing mutes
*Issuing bans
*Deleting player claims when necessary

**Player Engagement:** P L A Y E R E N G A G E M E N T I S Y O U R T O P P R I O R I T Y
Playerbase makes a server what it is, and it's a huge part of your job to contribute to it. There will be nice players, asshole players, quiet players, outgoing players, etc., and it's your job to connect with all of them. The number one way to do this, is be active in chat. Players will perceive you through the way you portray yourself in chat, and the way players perceive you, is how they perceive the server. 

**CoreProtect:** Coreprotect is our block logging/grief repairing plugin. This is the system you will use when responding to grief, confirming ownership, reinstating stolen items, and a variety of other things, so this is one you want to be familiar with.
 
/co inspect (/co i) This command activates 'inspect' mode. You can bind this tool to any block and it will allow you to inspect the placements/destructions/uses of any block.
 
/co rollback (/co rb) This is the standard rollback command and the one you will use when fixing simple griefs.

Following arguments available:

* user:<player> --- Use this argument to specify the player activity you want to revert.

* time:<time([s]econd,[m]inute,[h]our,[d]ay)>              Use this argument to specify how long ago you want to revert the activity back to.

* radius:<radius in blocks> Use this to specify the size of the area you want to revert activity back to.

* action:<action> Use to restrict your command to search for a specific action--- Ex:

* block >> blocks placed/broken

* +block >> blocks placed

* -block >> blocks broken

* container >> items put/taken from a chest/furnace/shulker/etc.

* +container >> items put in a chest/furnace/shulker/etc.

* -container >> items taken from a chest/furnace/shulker/etc.
 
Full Command E.g.

* /co rb time:1h user:Sikur action:-container -- Will add the items taken, back into the chest
* /co rb time:1h user:Sikur action:+container -- Will remove the items placed into the chest.
*Before using the interactive block rollback commands, make sure you practice with a long-time Bro or Admin first to ensure a player doesn't lose their items because of a mistake YOU made.
 
/co lookup (/co l) This command allows you to look up player activity regarding the following arguments:

* kill >> mobs/animals killed

* chat >> messages sent in chat

* command >> commands used

* session >> player logins/logouts

* +session >> player logins

* -session >> player logouts

* blocks: <block> Use to restrict your command to search for a specific block in a lookup/rollback

* exclude:<block> Use to exclude a specific block from your command in a lookup/rollback
 
Other Commands:

* /co restore (/co rs) Use this to restore a rollback created by a crash/other non-manual reverts of progress.
* /co undo Use this to revert a rollback.

